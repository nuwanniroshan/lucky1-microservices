# Lucky Microservices
version 1.0.0

## Sections
- **web** 
    - port `*3000*`
    - url `*http:localhost:3000*`
    - nginxUrl `*http:localhost:8080*`

- **api**
    - port `*4000*`
    - url `*http:localhost:4000*`
    - nginxUrl `*http:localhost:8080/api/*`
    - sample end pont GET:`*http:localhost:8080/api/people?q=nn*`

- **auth**
    - port `*5000*`
    - url `*http:localhost:5000*`
    - nginxUrl `*http:localhost:8080/auth/*`
    - sample end pont POST:`*http:localhost:8080/auth/login*` BODY `{ username:'administration', password: 'admin@123'}`

- **nginx**
    - port `*8080*`
    - url `*http://localhost:8080*`

## Pre-requests
- Docker
- NodeJs
- ReactJs
    - Install *`npx create-react-app my-app`*

## Build and Run
Execute *`docker-compose up --build`*

## Run without build
Execute *`docker-compose up`*


# Docker
## Docker Basic Commands
- *`docker ps`* List all runing images
- *`docker images`* List all images
- *`docker system prune`* Delete all unused containers and images
- *`docker build -t lucky-web .`* Build specific app
- *`docker run -it -p 3000:3000 luckyweb`*

