class AuthenticationService {
    constructor() {

    }

    login(username, password) {
        return new Promise((resolve, reject) => {
            if(username === 'administrator' && password === 'admin@123') {
                resolve({
                    token : this.generateToken(20)
                });
            }else {
                reject('Invalid username or password');
            }
        });
    }

    logout(token) {
        return new Promise((resolve, reject) => {
            resolve();
        });
    }

    generateToken(length) {
        let result           = '';
        const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;

        for (let i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
     }
}

module.exports = new AuthenticationService();