const express = require('express');
const bodyParser = require('body-parser');
const Logger = require('./utils/logger');
const LoginRouter = require('./routes/login');

const app = express();
const port = 5000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(LoginRouter);

app.use((err, req, res, next) => {
    if(err.isBoom){
        res.status(err.output.statusCode).send(err.output.payload);
    }else{
        res.status(500).send('Something broke!');
    }
});

app.listen(port, () => console.log(`Auth service on port ${port}!`))