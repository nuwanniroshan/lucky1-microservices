const AuthenticationService = require('../services/authentication-service');
const ValidationUtil = require('../utils/validation');

class LoginController {
    constructor() {

    }

    login(req, res) {
        ValidationUtil.isNotNullOrEmpty(req.body.username, 'Username');
        ValidationUtil.isNotNullOrEmpty(req.body.password, 'Password');

        AuthenticationService.login(req.body.username, req.body.password)
        .then((response) => {
            return res.json(response);
        }).catch((ex) => {
            return res.status(401).json({error : ex});
        });
    }

    logout(req, res) {

    }
}

module.exports =  new LoginController();