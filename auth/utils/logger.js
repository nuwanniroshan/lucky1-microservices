class Logger {
    constructor() {

    }

    log(err, req, res, next) {
        console.error(err.stack);
        next(err);
    }
}

module.exports = (new Logger());