const boom = require('boom');

class Validation {
    constructor() {

    }

    isNotNullOrEmpty(str, prop) {
        if (!str) {
            throw boom.badRequest(`missing ${prop}`);
        }
    }
}

module.exports = new Validation();