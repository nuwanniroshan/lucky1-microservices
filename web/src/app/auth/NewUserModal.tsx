import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, Form, Row, Col, FormGroup, Label, Input, CustomInput, ModalFooter, Button, Table } from 'reactstrap';

import SubmitButton from './../common/custom-controls/SubmitButton';

interface NewUserProps {
    isNewUser: boolean;
}

interface Permission {
    type: string;
    add: boolean;
    edit: boolean;
    view: boolean;
    delete: boolean;
    [key: string]: any;
}

interface NewUser {
    isAllPermisionChecked: boolean;
    permissions: Array<Permission>;
    identificationPermission: number;
    firstName: string;
    lastName: string;
    email: string;
    isMerchantUser: boolean;
    merchantId: number;
    password: string;
    confirmPassword: string;
    [key: string]: any;
}

interface NewUserModalState {
    showModal: boolean;
    newUser: NewUser;
}

class NewUserModal extends Component<NewUserProps, NewUserModalState> {

    // API data
    permissions = [
        {
            type: 'Lottery',
            add: true,
            edit: false,
            view: false,
            delete: true
        },
        {
            type: 'Draw',
            add: false,
            edit: true,
            view: false,
            delete: true
        }
    ];

    constructor(props: NewUserProps) {
        super(props);

        this.state = {
            showModal: false,
            newUser: {
                firstName: '',
                lastName: '',
                email: '',
                isMerchantUser: false,
                identificationPermission: 0,
                merchantId: 0,
                password: '',
                confirmPassword: '',
                isAllPermisionChecked: true,
                permissions: props.isNewUser ? this.getModifiedPermissions(this.permissions, true) : this.permissions
            }
        }
    }

    private getModifiedPermissions(permissions: Array<any>, isAllPermisionChecked: boolean): Array<Permission> {
        permissions.forEach(permission => {
            permission.add = isAllPermisionChecked;
            permission.view = isAllPermisionChecked;
            permission.edit = isAllPermisionChecked;
            permission.delete = isAllPermisionChecked;
        });

        return permissions;
    }

    private checkAllPermissionsChecked(permissions: Array<any>): boolean {
        let isAllChecked = true;
        permissions.forEach(permission => {
            if (!permission.add || !permission.view || !permission.edit || !permission.delete) {
                isAllChecked = false;
            }
            if (!isAllChecked) return;
        });
        return isAllChecked;
    }

    handleShow = () => {
        this.setState({ showModal: true });
    }

    handleClose = () => {
        this.setState({
            showModal: false,
            newUser: {
                firstName: '',
                lastName: '',
                email: '',
                isMerchantUser: false,
                identificationPermission: 0,
                merchantId: 0,
                password: '',
                confirmPassword: '',
                isAllPermisionChecked: true,
                permissions: []
            }
        });
    }

    handleAllPermisionChange = () => {
        let newUser = { ...this.state.newUser };
        newUser.isAllPermisionChecked = !newUser.isAllPermisionChecked;
        newUser.permissions = this.getModifiedPermissions(newUser.permissions, newUser.isAllPermisionChecked)

        this.setState({
            newUser
        });
    }

    handlePermissionChange = (index: number, permissionType: string) => {
        let newUser = { ...this.state.newUser };
        newUser.permissions[index][permissionType] = !newUser.permissions[index][permissionType];
        newUser.isAllPermisionChecked = this.checkAllPermissionsChecked(newUser.permissions);

        this.setState({
            newUser
        });
    }

    handleMerchantCheckChange = () => {
        let newUser = { ...this.state.newUser };
        newUser.isMerchantUser = !newUser.isMerchantUser;

        this.setState({
            newUser
        });
    }

    handleFormValueChange = (event: any) => {
        let newUser = { ...this.state.newUser };
        newUser[event.target.name] = event.target.value;
        this.setState({
            newUser
        });
    }

    handleSubmit = () => {
        console.log(this.state.newUser)
    }

    componentDidMount = () => {
        // TODO: Call API if any
    }

    render() {
        const { showModal, newUser } = this.state;
        const isInvalid = newUser.email === '' || newUser.firstName === '' || newUser.lastName === '' || newUser.password === '' || (newUser.password !== newUser.confirmPassword);

        return (
            <Modal size="lg" isOpen={showModal} toggle={this.handleClose}>
                <ModalHeader>Add New User</ModalHeader>
                <ModalBody>
                    <Form>
                        <Row>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="firstName">First Name</Label>
                                    <Input type="text" name="firstName" id="firstName" onChange={this.handleFormValueChange} placeholder="First Name" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="lastName">Last Name</Label>
                                    <Input type="text" name="lastName" id="lastName" onChange={this.handleFormValueChange} placeholder="Last Name" />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="email">Email</Label>
                                    <Input type="email" name="email" id="email" onChange={this.handleFormValueChange} placeholder="Email Address" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="identificationPermission">NIC / Mobile Number Permission</Label>
                                    <Input type="select" name="identificationPermission" id="identificationPermission">
                                        <option>Do not Show</option>
                                        <option>Show whole Number</option>
                                        <option>Show only last 4 letters</option>
                                    </Input>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="isMerchantUser"> </Label>
                                    <CustomInput type="switch" id="isMerchantUser" name="isMerchantUser" label="Merchant User" onChange={this.handleMerchantCheckChange} />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="merchantSelect">Select Merchant</Label>
                                    <Input type="select" name="select" id="merchantSelect">
                                        <option>Dialog Axiata</option>
                                    </Input>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={9}>
                                <FormGroup>
                                    <Label>User Permission</Label>
                                    <CustomInput type="switch" id="isAllPermisionChecked" name="isAllPermisionChecked" checked={newUser.isAllPermisionChecked} onChange={this.handleAllPermisionChange} label="All Permission" />
                                    <Table>
                                        <thead>
                                            <tr>
                                                <th className="border-0"></th>
                                                <th className="border-0">Add</th>
                                                <th className="border-0">Edit</th>
                                                <th className="border-0">View</th>
                                                <th className="border-0">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {newUser.permissions.map((value, index) => {
                                                return <tr key={index}>
                                                    <td>{value.type}</td>
                                                    <td><CustomInput id={"add" + index} type="checkbox" checked={value.add} onChange={(e) => this.handlePermissionChange(index, 'add')} /> </td>
                                                    <td><CustomInput id={"edit" + index} type="checkbox" checked={value.edit} onChange={(e) => this.handlePermissionChange(index, 'edit')} /> </td>
                                                    <td><CustomInput id={"view" + index} type="checkbox" checked={value.view} onChange={(e) => this.handlePermissionChange(index, 'view')} /> </td>
                                                    <td><CustomInput id={"delete" + index} type="checkbox" checked={value.delete} onChange={(e) => this.handlePermissionChange(index, 'delete')} /> </td>
                                                </tr>
                                            })}
                                        </tbody>
                                    </Table>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="password">Password</Label>
                                    <Input type="password" name="password" id="password" onChange={this.handleFormValueChange} />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="confirmPassword">Confirm Password</Label>
                                    <Input type="password" name="confirmPassword" id="confirmPassword" onChange={this.handleFormValueChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    {/* <Button color="primary" type="submit" disabled={isInvalid} onClick={this.handleSubmit}>Submit</Button>{' '} */}
                    <SubmitButton isInvalid={isInvalid} onSubmit={this.handleSubmit}></SubmitButton>
                    <Button color="secondary" onClick={this.handleClose}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}

export default NewUserModal;