import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container, Form, Input, Button } from 'reactstrap';

class ChangePassword extends Component {

    render() {
        return (
            <Container className="container text-center">
                <div className="pb-2 pt-5  text-center">
                    <img className="d-block mx-auto mt-2 mb-2" src="" alt="" width="72" height="72" />
                    <h1 className="mb-5 pb-2">Lucky1</h1>
                    <h3>Change password</h3>
                </div>
                <div className="row justify-content-center">
                    <div className="col col-sm-8 col-md-6 col-lg-4">
                        <Form className="form-login" autoComplete="off">
                            <Input type="password" id="currentPassword" className="form-control mb-3" placeholder="Current Password" required autoFocus />
                            <Input type="password" id="newPassword" className="form-control mb-3" placeholder="New Password" required />
                            <Input type="password" id="confirmPassword" className="form-control mb-3" placeholder="Confirm Password" required />
                            <Button color="primary" block type="submit">Change password</Button>
                            <Link to="/" className="btn btn-light btn-block">Back</Link>

                            <p className="mt-5 mb-3 text-muted">&copy; Lucky1</p>
                        </Form>
                    </div>
                </div>
            </Container>
        );
    }
}

export default ChangePassword;
