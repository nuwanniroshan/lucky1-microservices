import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container, Form, Input, Button } from 'reactstrap';
import { History } from 'history';

import logo from './../../assets/lucky-icon.svg';

interface LoginProps {
  history: History
}

interface LoginState {
  submitted?: boolean;
  username: string;
  password: string;
}

class Login extends Component<LoginProps, LoginState> {

  onSubmit = () => {
    this.props.history.push('/dashboard');
  }

  render() {

    return (
      <Container className="container text-center">
        <div className="pb-2 pt-5 text-center">
          <img className="d-block mx-auto mt-2 mb-2" src={logo} alt="lucky1-logo" width="72" height="72" />
          <h1 className="mb-5 pb-2">Lucky1</h1>
          <h3>Login</h3>
        </div>
        <div className="row justify-content-center">
          <div className="col col-sm-8 col-md-6 col-lg-4">
            <Form className="form-login" autoComplete="off" onSubmit={this.onSubmit}>
              <Input name="username" type="text" id="username" className="form-control mb-3" placeholder="Username" autoFocus />
              <Input name="password" type="password" id="password" className="form-control mb-3" placeholder="Password" />

              <Button color="primary" type="submit">Login</Button>

              {/* {error && <p className="text-danger"><br />{error.message}</p>} */}
              <p className="mt-4 mb-5 text-muted">
                <Link to="/change-password">Forgot your username or password?</Link>
              </p>
              <p className="mt-5 mb-3 text-muted">&copy; Lucky1</p>
            </Form>
          </div>
        </div>
      </Container>
    );
  }
}

export default Login;
