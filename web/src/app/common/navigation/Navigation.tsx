import React, { Component } from 'react';
import { Nav, NavItem, NavLink } from 'reactstrap';


class Navigation extends Component {

    render() {
        return (
            <div>
                <Nav vertical>
                    <NavItem>
                        <NavLink href="#">Dashboard</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="#">Draw</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="#">Subscribers</NavLink>
                    </NavItem>
                </Nav>
            </div>
        );
    }
}

export default Navigation;
