import React, { Component } from 'react';
import { Button, Spinner } from 'reactstrap';

class SubmitButton extends Component<{ isInvalid: boolean, onSubmit: any }, { submitting: boolean }> {

    constructor(props: { isInvalid: boolean, onSubmit: any }) {
        super(props);
        this.state = {
            submitting: false
        }
    }

    handleSubmit = () => {
        this.setState({
            submitting: true
        });
        this.props.onSubmit();
    }

    render() {
        return (
            <Button color="primary" type="submit" disabled={this.props.isInvalid} onClick={this.handleSubmit}>
                {this.state.submitting ? <Spinner size="sm" color="light" /> : ''} Submit
            </Button>
        );
    }
}

export default SubmitButton;
