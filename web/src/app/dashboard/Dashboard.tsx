import React, { Component } from 'react';
import { Container, Button } from 'reactstrap';

import NewUserModal from '../auth/NewUserModal';
import Header from '../common/header/Header';

interface ModalProps {
}

interface ModalState {
}

class Dashboard extends Component<ModalProps, ModalState> {

    showModal: any;

    newUserModalRef = ({ handleShow }: NewUserModal) => {
        this.showModal = handleShow;
    }

    openModal = () => {
        this.showModal();
    }

    render() {
        return (
            <Container fluid className="px-0">
                <Header></Header>
                <div className="pb-2 pt-5 text-center">
                    <h1 className="mb-5 pb-2">Dashboard</h1>
                    <Button color="success" onClick={this.openModal}><i className="fas fa-plus"></i> Add New User</Button>
                </div>
                <NewUserModal ref={this.newUserModalRef} isNewUser={true}></NewUserModal>
            </Container>);
    }
}

export default Dashboard;
