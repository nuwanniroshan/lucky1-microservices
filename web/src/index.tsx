import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.scss';
import * as serviceWorker from './serviceWorker';
import Dashboard from './app/dashboard/Dashboard';
import ChangePassword from './app/auth/ChangePassword';
import Login from './app/auth/Login';

const routing = (
    <Router>
        <Route exact path="/" component={Login} />
        <Route path='/change-password' component={ChangePassword} />
        <Route path="/dashboard" component={Dashboard} />
    </Router>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
