const PeopleService = require('../services/people-service');

class PeopleController {
    constructor() {

    }

    getAll(req, res) {
        let keyword = req.query.q;
        let people = [];

        if(keyword) {
            keyword = keyword.toLowerCase();

            people = PeopleService.getAll().filter((p) => {
                return p.toLowerCase().indexOf(keyword) > -1;
            });
        }else {
            people = PeopleService.getAll();
        }

        res.json(people);
    }
}

module.exports = new PeopleController();