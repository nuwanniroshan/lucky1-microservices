const express = require('express');
const PeopleRouter = require('./routes/people');

const app = express();
const port = 4000;

app.use('/people', PeopleRouter);

app.listen(port, () => console.log(`Api service on port ${port}!`))