const express = require('express');
const router = express.Router();
const PeopleController = require('../controllers/people');

router.get('/', PeopleController.getAll);

module.exports = router;